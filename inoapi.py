from requests_oauthlib import OAuth2Session
from flask import Flask, request, redirect, session, url_for, abort, render_template, g
from flask.json import jsonify
import os

from urllib.parse import urlencode
from secrets import token_urlsafe

from lib.InoreaderApiRouter import InoreaderApiRouter
from lib.database import Database, User

#  INIT DATABASE

db = Database("sqlite:///db.sqlite")

#  INIT FLASK
app = Flask(__name__)
inoreader_api = InoreaderApiRouter()

STATE = token_urlsafe(10)
app.secret_key = STATE

OATH_CB = "https://localhost:5000/callback"

client_id = '1000000120'
client_secret = 'T137y87yMNysPfiZ8d4uSEuslRxFnqDg'


@app.route("/")
def demo():
    """Step 1: User Authorization.

    Redirect the user/resource owner to the OAuth provider (i.e. Github)
    using an URL with a few key OAuth parameters.
    """
    inoreader = OAuth2Session(client_id,
                              redirect_uri=OATH_CB,
                              scope="read",
                              state=STATE)
    authorization_url, state = inoreader.authorization_url(inoreader_api.get_auth("auth"))

    # State is used to prevent CSRF, keep this for later.
    session['oauth_state'] = STATE
    return redirect(authorization_url)


# Step 2: User authorization, this happens on the provider.

@app.route("/callback", methods=["GET"])
def callback():
    """ Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """
    authorization_code = request.args.get("code")
    state = request.args.get("state")

    if state == session.get("oauth_state"):

        inoreader = OAuth2Session(client_id, state=session['oauth_state'],
                                  redirect_uri=OATH_CB)
        token = inoreader.fetch_token(inoreader_api.get_auth("token"),
                                      code=authorization_code,
                                      client_secret=client_secret,
                                      scope="",
                                      headers={"Content-type": "application/x-www-form-urlencoded"})

        # At this point you can fetch protected resources but lets save
        # the token and show how this is done from a persisted token
        # in /profile.
        session['oauth_token'] = token
        return redirect(url_for('.profile'))

    else:
        return abort(404, description="Invalid state.")


@app.route("/profile", methods=["GET"])
def profile():
    """Fetching a protected resource using an OAuth 2 token.
    """
    inoreader = OAuth2Session(client_id, token=session.get('oauth_token'))
    headers = {"Authorization": "Bearer {}".format(session.get("oauth_token"))}

    user_profile = inoreader.get(inoreader_api.get_route("user_info"), headers=headers)
    user_profile = user_profile.json()

    user = User(
        userId=user_profile["userId"],
        userName=user_profile["userName"],
        userEmail=user_profile["userEmail"]
    )
    db.session.add(user)
    db.session.commit()
    g.username = user_profile["userName"]

    unread_count = inoreader.get(inoreader_api.get_route("count_unread"), headers=headers)
    unread_count = unread_count.json()
    unread = {item["id"]: item["count"] for item in unread_count["unreadcounts"]}

    subscriptions = inoreader.get(inoreader_api.get_route("list_subscription"), headers=headers)
    subscriptions = subscriptions.json()
    subscriptions = subscriptions["subscriptions"]
    for subscription in subscriptions:
        subscription_id = subscription["id"]
        subscription["unread"] = unread[subscription_id]

    g.subscriptions = subscriptions

    return render_template("feeds.html")


@app.route("/feeds/<path:feed_id>", methods=["GET"])
def stream_feeds(feed_id):
    inoreader = OAuth2Session(client_id, token=session.get('oauth_token'))
    headers = {"Authorization": "Bearer {}".format(session.get("oauth_token"))}

    query_string = {"n": 25}
    if request.args.get("continuation"):
        query_string["c"] = request.args.get("continuation")
    query_string = urlencode(query_string)

    url = inoreader_api.get_route("content_stream") + "/" + feed_id + "?" + query_string

    feeds_stream = inoreader.get(url, headers=headers)
    feeds_stream = feeds_stream.json()

    d = {
        "stream_id": feeds_stream["id"],
        "stream_title": feeds_stream["title"],
        "items": feeds_stream["items"]
    }

    if "continuation" in feeds_stream.keys():
        d["continuation"] = feeds_stream["continuation"]

    return jsonify(d)


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = "1"

    app.secret_key = STATE
    app.run(ssl_context="adhoc", debug=True)
