from sqlalchemy import create_engine, Column, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#  INIT SQLALCHEMY

Base = declarative_base()


class Database:
    def __init__(self, connection):
        self.connection = connection
        self.engine = create_engine(connection)
        self.session = sessionmaker(bind=self.engine)
        self.base = declarative_base()
        self.base.metadata.create_all(self.engine)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    userName = Column(String)
    userEmail = Column(String)
    userId = Column(Integer)

    def __repr__(self):
        return "<User(userName={}, userEmail={}, userId={})>".format(self.userId, self.userEmail, self.userId)


class Feed(Base):
    __tablename__ = 'feeds'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    author = Column(String)
    is_starred = Column(Boolean)
    rating = Column(Integer)

    def __repr__(self):
        return "<Feed(title={}, is_starred={}, rating={})>".format(self.title, self.is_starred, self.rating)
