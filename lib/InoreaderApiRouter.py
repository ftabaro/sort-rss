from urllib.parse import urljoin


class InoreaderApiRouter:

    #  +--------+------------------------+-------------------------+
    #  |  Zone  |   Free account limit   |    Pro account limit    |
    #  +--------+------------------------+-------------------------+
    #  | Zone 1 | 5,000 requests per day | 50,000 requests per day |
    #  +--------+------------------------+-------------------------+
    #  | Zone 2 | 1,000 requests per day | 10,000 requests per day |
    #  +--------+------------------------+-------------------------+

    def __init__(self):
        #  limits = {"1": 5000, "2": 1000}
        self.root = "https://www.inoreader.com/reader/api/0/"
        self.routes = {
            "token": {"endpoint": "token", "zone": 1},
            "save_prefs": {"endpoint": "save-user-pref", "zone": 2},
            "user_info": {"endpoint": "user-info", "zone": 1},
            "list_pref": {"endpoint": "preference/list", "zone": 1},
            "list_stream": {"endpoint": "preference/stream/list", "zone": 1},
            "set_stream": {"endpoint": "preference/stream/set", "zone": 2},
            "list_tag": {"endpoint": "tag/list", "zone": 1},
            "list_subscription": {"endpoint": "subscription/list", "zone": 2},
            "mark_all_read": {"endpoint": "mark-all-as-read", "zone": 2},
            "undo_mark_all_read": {"endpoint": "mark-all-as-read-undo", "zone": 2},
            "quickadd_subscription": {"endpoint": "subscription/quickadd", "zone": 2},
            "edit_subscription": {"endpoint": "subscription/edit", "zone": 2},
            "count_unread": {"endpoint": "unread-count", "zone": 1},
            "edit_tag": {"endpoint": "edit-tag", "zone": 2},
            "rename_tag": {"endpoint": "rename-tag", "zone": 2},
            "content_stream": {"endpoint": "stream/contents", "zone": 1},
            "content_stream_items": {"endpoint": "stream/items/contents", "zone": 1},
            "stream_items_ids": {"endpoint": "stream/items/ids", "zone": 1}
        }

        self.oauth = {
            "auth": "https://www.inoreader.com/oauth2/auth",
            "token": "https://www.inoreader.com/oauth2/token"
        }

    def get_route(self, key):
        if key not in self.routes.keys():
            raise KeyError("Could not determine endpoint. Invalid key: {}".format(key))
        return urljoin(self.root, self.routes[key]["endpoint"])

    def get_auth(self, key):
        if key not in self.oauth.keys():
            raise KeyError("Could not determine Oauth endpoint")
        return self.oauth[key]
