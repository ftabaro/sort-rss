import json
from typing import Dict, List
import pathlib
from bs4 import BeautifulSoup
import feedparser
from .rssObject import RSSObject
#import requests

import pprint
#APPID = '1000000120'
#APPKEY = 'T137y87yMNysPfiZ8d4uSEuslRxFnqDg'


class DataImporter:
    def __init__(self,
                 subscription_path: str,
                 starred_path: str,
                 subscription_folder_names: List[str]):

        self.subscription_path = subscription_path
        self.starred_path = starred_path

        self.subscriptions = self.__parse_subscription_xml()
        self.feed_url = self.set_feed_url(subscription_folder_names)

    def __parse_subscription_xml(self):
        fh = open(self.subscription_path, 'r')
        soup = BeautifulSoup(fh, 'xml')
        fh.close()
        category = 'random'
        subscriptions = {category: []}
        for outline in soup.find_all('outline'):
            children = [child for child in outline.children]
            if len(children) > 0:
                category = outline['title'].lower().replace(' ', '_')
                subscriptions[category] = []
                continue
            subscriptions[category].append(outline)

        return subscriptions

    def set_feed_url(self, subscription_folder_names: List[str]):
        feed_url = []
        for folder_name in subscription_folder_names:
            feed_url += [feed['xmlUrl'] for feed in self.subscriptions[folder_name]]
        return feed_url

    def get_feeds(self):
        for url in self.feed_url:
            feed = feedparser.parse(url)
            for entry in feed.entries:
                myRss = {
                    'title': entry.title,
                    'url': entry.link,
                }

                description = None
                if hasattr(entry, 'description'):
                    description = entry.description
                elif hasattr(entry, 'summary'):
                    description = entry.summary
                if description is not None:
                    myRss['description'] = description

                date = None
                if hasattr(entry, 'published_parsed'):
                    date = "%d/%02d/%02d" % (entry.published_parsed.tm_year,
                                             entry.published_parsed.tm_mon,
                                             entry.published_parsed.tm_mday)
                elif hasattr(entry, 'updated_parsed'):
                    date = "%d/%02d/%02d" % (entry.updated_parsed.tm_year,
                                             entry.updated_parsed.tm_mon,
                                             entry.updated_parsed.tm_mday)
                if date is not None:
                    myRss['date'] = date

                if hasattr(entry, 'authors'):
                    try:
                        myRss['author'] = [x['name'] for x in entry['authors']]
                    except KeyError:
                        pass

                e = RSSObject(myRss)

                yield e

    def get_starred(self):
        path = pathlib.Path(self.starred_path)
        if not path.exists():
            raise ValueError("%s does not exist.".format(path))

        fh = path.open("r")
        items = json.load(fh)
        fh.close()

        items = items['items']

        from pprint import pprint as pp
        for i in range(10):
            pp(items[i])

        for item in items:
            yield RSSObject(item)
