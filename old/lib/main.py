from random import randint, seed

from .dataImporter import DataImporter
#from .modelBuilder import ModelBuilder

import logging
import os
import datetime

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.INFO if not os.environ.get('DEBUG') else logging.DEBUG)
main_logger = logging.getLogger('__main__')
main_logger.setLevel(logging.INFO if not os.environ.get('DEBUG') else logging.DEBUG)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

stream_formatter = logging.Formatter('%(levelname)s %(name)s %(message)s')
console_handler.setFormatter(stream_formatter)

package_timestamp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
if os.path.isdir('logs'):
    file_handler = logging.FileHandler(
        os.path.join('logs', '{}_{}.log'.format(__name__, package_timestamp)), mode='a')
    file_handler.setLevel(logging.INFO if not os.environ.get('DEBUG') else logging.DEBUG)
    file_handler.setFormatter(
        logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s'))
    module_logger.addHandler(file_handler)
    main_logger.addHandler(file_handler)

module_logger.addHandler(console_handler)
main_logger.addHandler(console_handler)

def main():

    seed(1)

    importer = DataImporter('/data/subscriptions.xml',  # subscription xml
                            "/data/starred.json",  # starred json
                            ['npg', 'pnas', 'sciencemag', 'cell', 'chromatin'])  # folder names of feeds to fetch

    starred = [feed for feed in importer.get_starred()]

    feeds = [feed for feed in importer.get_feeds()]

    main_logger.info("Imported {} new feeds.".format(len(feeds)))
    for i in range(min(10, len(feeds))):
        print(feeds[i])

    main_logger.info("Imported {} starred feeds.".format(len(starred)))
    for i in range(min(10, len(starred))):
        print(starred[i])

    titles = [star.title for star in starred]
    is_starred = [1 for i in range(len(starred))]
    urls = [star.url for star in starred]

    tokenizer = Tokenizer(oov_token="<OOV>")
    tokenizer.fit_on_texts(titles)

    word_index = tokenizer.word_index
    sequences = tokenizer.texts_to_sequences(titles)
    padded = pad_sequences(sequences, padding="post", truncating="post")

    # random_set = []
    # for i in range(200):
    #     random_index = randint(0, len(feeds) - 1)
    #     random_set.append(feeds.pop(random_index))
    #
    # positive_validation_set = []
    # negative_validation_set = []
    # for i in range(50):
    #     random_index_starred = randint(0, len(starred) - 1)
    #     positive_validation_set.append(starred.pop(random_index_starred))
    #
    #     random_index_random = randint(0, len(random_set) - 1)
    #     negative_validation_set.append(random_set.pop(random_index_random))
    #
    # model = ModelBuilder()
    # model.train(starred, random_set)
    #
    # model.do_validation(positive_validation_set, negative_validation_set)

    # model.predict(feeds)


