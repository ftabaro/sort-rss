from typing import List, Dict, AnyStr
from bs4 import BeautifulSoup


class RSSObject:
    def __init__(self, item: Dict):
        self.original_title = item["title"]
        self.title = self.__get_title(item)
        self.url = self.__get_url(item)
        self.author = self.__get_author(item)
        self.summary = self.__get_summary(item)

    def __repr__(self):
        return "RSSObject<title={} - url={}>".format(self.title, self.url)

    @staticmethod
    def __get_title(item: Dict) -> List[str]:
        title = item["title"].lower()
        return title

    @staticmethod
    def __get_url(item: Dict) -> str:
        if 'canonical' in item \
                and type(item['canonical']) is list:
            url = item["canonical"][0]["href"]
        elif 'url' in item:
            url = item['url']
        else:
            raise KeyError("Cold not find url in item.")
        return url

    @staticmethod
    def __get_summary(item: Dict) -> List[str]:
        if 'summary' not in item or 'description' not in item:
            return []

        if 'summary' in item \
                and 'content' in item['summary'] \
                and type(item['summary']['content'] is list):
            summary = item["summary"]["content"].lower()

        elif 'description' in item:
            summary = item['description']
        else:
            raise KeyError('Could not find RSS feed description.')

        if bool(BeautifulSoup(summary, "html.parser").find()):
            soup = BeautifulSoup(summary, features="html.parser")
            summary = soup.get_text().strip()
        else:
            summary = summary.strip()

        return summary

    @staticmethod
    def __get_author(item: Dict) -> AnyStr:
        author = None
        if 'author' in item:
            author = item['author']
        return author
