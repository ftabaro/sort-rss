import nltk
import pprint
import re
from typing import List, Optional

from .rssObject import RSSObject


class ModelBuilder:
    def __init__(self):
        # TODO: implement logging
        # self.positive_examples: List[RSSObject] = []
        # self.negative_examples: List[RSSObject] = []
        # self.testing: List[RSSObject] = []
        # self.build_sets()

        self.classifier: Optional[nltk.NaiveBayesClassifier] = None
        self.fdist = None
        self.word_features = None

        self.blacklist = ["published", "online", "height=", "width=", "inoreader",
                          "twitter", "facebook", "follow", "Published"]

    def __get_text(self, target: List[RSSObject]):

        text = []
        for item in target:
            text += item.title + item.summary
        text = [word for word in text if len(word) > 4
                and word not in self.blacklist
                and not word.startswith("doi")
                and not re.findall(r'^//(?:www)?.+\..+$', word)]
        return text

    def __build_word_features(self, target: List[RSSObject]):
        text = self.__get_text(target)

        self.fdist = nltk.FreqDist(text)
        word_features = sorted(self.fdist.items(), key=lambda x: -x[1])[:300]
        self.word_features = [x[0] for x in word_features]

    def __build_bigrams(self, target: List[RSSObject]):
        bigrams = set([])
        for item in target:
            bgrms_title = nltk.bigrams(item.title)
            bgrms_summary = nltk.bigrams(item.summary)
            bgrms = set([item for item in bgrms_title] + [item for item in bgrms_summary])
            bigrams = bigrams.union(bgrms)

        to_remove = []
        for bigram in bigrams:
            if bigram[0] not in self.word_features:
                to_remove.append(bigram)

        for item in to_remove:
            bigrams.remove(item)

        self.bigrams = bigrams

    def __build_collocations(self, target: List[RSSObject]):
        text = nltk.Text(self.__get_text(target))
        bigram_measures = nltk.collocations.BigramAssocMeasures()
        finder = nltk.collocations.BigramCollocationFinder.from_words(text)
        finder.apply_freq_filter(2)
        self.collocations = finder.nbest(bigram_measures.pmi, 50)

    def __build_features_matrix(self, target: List[RSSObject]):

        # tags = nltk.pos_tag(text)
        # tags_dict = {}
        # for tag_tuple in tags:
        #     tags_dict[tag_tuple[0]] = tag_tuple[1]

        for item in target:
            feat = {}
            for word in self.word_features:
                feat['title_contains({})'.format(word)] = word in item.title
                feat['summary_contains({})'.format(word)] = word in item.summary
                feat['fdist({})'.format(word)] = self.fdist[word]

            bigram_title = nltk.bigrams(item.title)
            bigram_summary = nltk.bigrams(item.summary)
            for bigram in self.bigrams:
                feat['title_contains({})'.format('-'.join(bigram))] = bigram in bigram_title
                feat['summary_contains({})'.format('-'.join(bigram))] = bigram in bigram_summary

            for collocation in self.collocations:
                feat['title_contains_collocation({})'.format('-'.join(collocation))] = collocation in bigram_title
                feat['summary_contains_collocation({})'.format('-'.join(collocation))] = collocation in bigram_summary

            yield feat

    def __label_feeds(self, positive_examples: List[RSSObject], negative_examples: List[RSSObject]):
        return [(item, 'star') for item in self.__build_features_matrix(positive_examples)] + \
                [(item, 'no star') for item in self.__build_features_matrix(negative_examples)]

    def train(self, positive_examples: List[RSSObject], negative_examples: List[RSSObject]):
        corpus = positive_examples + negative_examples
        self.__build_word_features(corpus)
        self.__build_bigrams(corpus)
        self.__build_collocations(corpus)

        train = self.__label_feeds(positive_examples, negative_examples)
        self.classifier = nltk.NaiveBayesClassifier.train(train)

    def predict(self, data: List[RSSObject]):
        test = [feature for feature in self.__build_features_matrix(data)]
        for i, item in enumerate(data):
            pred = self.classifier.classify(test[i])
            print(i, pred, item.original_title)

    def do_validation(self, positive_validation_set: List[RSSObject], negative_validation_set: List[RSSObject]):
        validation = self.__label_feeds(positive_validation_set, negative_validation_set)

        pprint.pprint(self.classifier.show_most_informative_features(10))
        print(nltk.classify.accuracy(self.classifier, validation))
