"""update users table

Revision ID: 9d885d5d463e
Revises: 
Create Date: 2020-08-25 17:05:16.677879

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9d885d5d463e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("users") as batch_op:
        batch_op.add_column(sa.Column("userName", sa.String))
        batch_op.add_column(sa.Column("userEmail", sa.String))
        batch_op.add_column(sa.Column("userId", sa.Integer))

        for c in ["name", "fullname", "nickname"]:
            batch_op.drop_column(c)


def downgrade():
    with op.batch_alter_table("users") as batch_op:
        batch_op.add_column(sa.Column("name", sa.String))
        batch_op.add_column(sa.Column("fullname", sa.String))
        batch_op.add_column(sa.Column("nickname", sa.String))

        for c in ["userName", "userEmail", "userId"]:
            batch_op.drop_column(c)
